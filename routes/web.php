<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@about');
Route::get('/contact', 'HomeController@contact');
Route::get('/courses_all', 'HomeController@courses');
Route::get('/detail/{id}', 'HomeController@detail');
Route::get('/payment/{id}', 'HomeController@payment')->middleware('auth');
Route::post('/checkout', 'HomeController@checkout')->middleware('auth');

Auth::routes();

// route admin 
Route::get('/dashboard', function() {
    return view('pages.admin.dashboard');
})->middleware(['auth','admin']);
Route::resource('category', 'CategoryController')->middleware(['auth','admin']);
Route::resource('course', 'CourseController')->middleware(['auth','admin']);
Route::get('bookings', 'BookingController@index')->middleware(['auth','admin']);