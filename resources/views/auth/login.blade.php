<!DOCTYPE html>
<html lang="en">
<head>
    <title>WebUni - Education</title>
    <meta charset="UTF-8">
    <meta name="description" content="WebUni Education Template">
    <meta name="keywords" content="webuni, education, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->   
    <link href="{{ asset('frontend/img/favicon.ico')}}" rel="shortcut icon"/>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">


</head>
<body>
    <section class="hero-section set-bg" data-setbg="{{ asset('frontend/img/bg.jpg')}}">
        <div class="col-lg-6">
            <section class="signup-section spad">
                <div class="signup-warp">
                    <div class="section-title text-white text-left">
                        <h2>Login</h2>
                        <p>Fill out the form below to sign up for WebUni. Already signed up? Then just <a class="link" href="/register" >Sign Up</a></p> 

                    </div>
                    <!-- signup form -->
                    <form class="signup-form" action="{{ route('login') }}" method="post">
                      @csrf
                        <input type="email" name="email" placeholder="Usernama">
                        @error('email')
                        <span class="text-danger" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <input type="password" name="password" placeholder="Password">
                        @error('password')
                        <span class="text-danger" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <p class="pw">Lupa Password?</p>
                        <input type="file" id="v-login">
                        <button type="submit" class="site-btn">LOGIN</button>
                        <input type="file" id="v-signup">
                        <button class="site-btn"><a href="/register" style="color: white;">SIGN UP</a></button>
                    </form>
                </div>
            </section>
        </div>
    </section>


    <!--====== Javascripts & Jquery ======-->
    <script src="{{ asset('frontend/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/mixitup.min.js') }}"></script>
    <script src="{{ asset('frontend/js/circle-progress.min.js') }}"></script>
    <script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>
</body>
</html>
