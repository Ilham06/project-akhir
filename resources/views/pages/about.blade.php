@extends('layouts.home')

@section('content')
<!-- Hero section -->
<section class="hero-section set-bg" data-setbg="{{ asset('frontend/img/bg.jpg')}}">
	<div class="container">
		<div class="hero-text text-white">
			<h2>Get The Best Free Online Courses</h2>
			<p>Get The Exacts Skills And Specialization You Need To Upgrade Skills. <br> Find Free Courses. Find Courses. Highlight: Webinars Available, Demo Available</p>
		</div>
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<form class="intro-newslatter">
					<input type="text" placeholder="Name">
					<input type="text" class="last-s" placeholder="E-mail">
					<button class="site-btn"><a href="/register" style="color: white;">Sign Up Now</a></button>
				</form>
			</div>
		</div>
	</div>
</section>
<section class="categories-section spad">
	<div class="container">
		<h2 style="text-align: center;padding-top: 10px;">About Us Courses</h2>
		<p style="margin-top: 10px;">Our courses have been created by experts who understand the challenges and barriers you face. Each course includes interactive elements, carefully curated resources, action research opportunities and moments for professional reflection.

			The resources and activities supplied have all been carefully selected by our team of expert trainers. We know that your time is valuable, so we have spent time selecting the highest quality resources and made sure that they are all stored in one easy place for you to access.
			
		We are aware that local context is important so, instead of providing you with one toolkit, each course includes a range of ideas and readings to give you the freedom to focus in on the aspect of outdoor learning and play that you find most important.</p>
	</div>
</section>
<section class="signup-section spad">
	<div class="signup-bg set-bg" data-setbg="{{ asset('frontend/img/signup-bg.jpg')}}"></div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6">
				<div class="signup-warp">
					<div class="section-title text-white text-left">
						<h2>Sign up to became a teacher</h2>
			
					</div>
					<!-- signup form -->
					<form class="signup-form">
						<input type="text" placeholder="Your Name">
						<input type="text" placeholder="Your E-mail">
						<input type="text" placeholder="Your Phone">
						<label for="v-upload" class="file-up-btn">Upload Course</label>
						<input type="file" id="v-upload">
						<button class="site-btn">Search Couse</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- signup section end -->


<!-- banner section -->
<section class="banner-section spad">
	<div class="container">
		<div class="section-title mb-0 pb-2">
			<h2>Join Our Community Now!</h2>
			<p>Let's Prove Itself! <br> That WebUni Can Helping You Develop Your Programming Skills</p>
		</div>
		<div class="text-center pt-5">
			<a href="/register" class="site-btn">Register Now</a>
		</div>
	</div>
</section>
<!-- banner section end -->
@endsection