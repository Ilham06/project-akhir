@extends('layouts.home')

@section('content')
<!-- Page info -->
<div class="page-info-section set-bg" data-setbg="{{ asset('frontend/img/bg.jpg')}}">
	<div class="container">
		<div class="site-breadcrumb">
			<a href="/">Home</a>
			<span>Courses</span>
		</div>
	</div>
</div>
<!-- Page info end -->


<!-- search section -->
<section class="search-section ss-other-page">
	<div class="container">
		<div class="search-warp">
			<div class="section-title text-white">
				<h2 class="mb-4"><span>Search your course</span></h2>
			</div>
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<!-- search form -->
					<form class="course-search-form">
						<input type="text" placeholder="Course">
						<input type="text" class="last-m" placeholder="Category">
						<button class="site-btn btn-dark">Search Couse</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- search section end -->
<section class="course-section spad">
	<div class="container">
		<div class="section-title mb-0">
			<h2>Courses</h2>
		</div>
	</div>
	<div class="course-warp">                                      
		<div class="row course-items-area">
			@foreach ($courses as $course)
			<!-- course -->
			<div class="mix col-lg-3 col-md-4 col-sm-6 finance">
				<div class="course-item">
					<div class="course-thumb set-bg" data-setbg="{{ asset('images/'.$course->thumbnail)}}">
						<div class="price">Price: Rp. {{ number_format($course->price) }}</div>
					</div>
					<div class="course-info">
						<div class="course-text">
							<a href="detail/{{ $course->id }}"><h5>{{ $course->name }}</h5></a>
							<p>{{ $course->category->name }}</p>
							<div class="students">{{ count($course->booking) }} Students</div>
						</div>
							{{-- <div class="course-author">
								<div class="ca-pic set-bg" data-setbg="{{ asset('frontend/img/authors/1.jpg')}}"></div>
								<p>William Parker, <span>Developer</span></p>
							</div> --}}
						</div>
					</div>
				</div>
				<!-- course -->
				@endforeach
			</div>
		</div>
	</section>
	@endsection