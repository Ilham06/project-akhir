<!DOCTYPE html>
<html lang="en">
<head>
    <title>WebUni - Education</title>
    <meta charset="UTF-8">
    <meta name="description" content="WebUni Education Template">
    <meta name="keywords" content="webuni, education, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->   
    <link href="{{ asset('frontend/img/favicon.ico')}}" rel="shortcut icon"/>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">


</head>
<body>
    <section class="hero-section set-bg" data-setbg="{{ asset('frontend/img/bg.jpg')}}">
        <div class="col-lg-6">
            <section class="signup-section spad">
                <div class="signup-warp">
                    <div class="section-title text-white text-left">
                        <form action="/checkout" method="post">
                            @csrf
                            <h2>WebUni Payment</h2>
                            <h3> Informasi Pelanggan</h3>
                            <p> Lengkapi form dibawah ini </p>
                            <p class=""email> Email </p>
                            <input type="hidden" name="course" value="{{ $course->id }}">
                            <input type="text" class="form-control" placeholder="Contoh:john@gmail.com" value="{{ Auth::user()->email }}">

                            <h5> Nama dan Nomor Whatsapp</h5>
                            <p> Nama Lengkap </p>
                            <input type="text" class="form-control" placeholder="Contoh:John Doe" value="{{ Auth::user()->name }}">
                            <p style="padding-top: 10px;"> Nomor Whatsaap </p>
                            <input type="number" class="form-control" placeholder="Contoh:085720xxxxxx" required>
                            <p> Note: Pastikan email dan nomor whatsaap yang kamu masukan adalan benar. Sebab kami akan gunakan data tersebut untuk mengirimkan voucher aktivitasi paket belajar kamu.</p>
                            <div style="text-align: left; font-weight: bold; font-size: 30px; margin-bottom: 20px;">
                                <img src="{{ asset('frontend/img/courses/1.jpg')}}" width="150px"> {{ $course->name }} <span>Rp. {{ number_format($course->price) }}</span>
                            </div>
                            <div>
                                <button type="submit" class="site-btn" style="margin-left: 50px;">Checkout</button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </section>


    <!--====== Javascripts & Jquery ======-->
    <script src="{{ asset('frontend/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/mixitup.min.js') }}"></script>
    <script src="{{ asset('frontend/js/circle-progress.min.js') }}"></script>
    <script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>
</body>
</html>