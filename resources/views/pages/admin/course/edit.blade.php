@extends('layouts.admin')

@section('title', 'Tambah Course')

@section('content')
<form action="{{ route('course.update', $course->id) }}" method="post" enctype="multipart/form-data">
	@method('put')
	@csrf
	<div class="form-group">
		<label>Nama Course</label>
		<input type="text" class="form-control" name="name" placeholder="Masukan Nama" value="{{ $course->name }}">
		@error('name')
			<small class="text-danger">{{ $message }}</small>
		@enderror
	</div>
	<div class="form-group">
		<label>Thumbnail</label>
		<input type="file" class="form-control" name="thumbnail">
		@error('thumbnail')
			<small class="text-danger">{{ $message }}</small>
		@enderror
	</div>
	<div class="form-group">
		<label>Harga</label>
		<input type="number" class="form-control" name="price" placeholder="Masukan Harga" value="{{ $course->price }}">
		@error('price')
			<small class="text-danger">{{ $message }}</small>
		@enderror
	</div>
	<div class="form-group">
		<label for="">Kategory</label>
		<select name="category_id" id="" class="form-control">
			@foreach ($categories as $category)
				<option value="{{ $category->id }}">{{ $category->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="form-group">
		<label for="desc" class="form-label">Keterangan</label>
		<input id="x" type="hidden" name="description" value="{{ $course->description }}">
		<trix-editor input="x"></trix-editor>
		@error('description')
			<small class="text-red">{{ $message }}</small>
		@enderror
	</div>
	<button type="submit" class="btn btn-success mt-2">Simpan</button>
</form>
@endsection