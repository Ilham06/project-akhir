@extends('layouts.admin')

@section('title', 'Daftar Course')

@section('content')
<a href="{{ route('course.create') }}" class="btn btn-success btn-sm mb-2">Tambah Data</a>
<table id="example1" class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>Nama Course</th>
      <th>Thumbnail</th>
      <th>Kategory</th>
      <th>Harga</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($courses as $course)
    <tr>
      <td>{{ $course->name }}</td>
      <td width="20%"><img width="100%" src="{{ asset('images/'.$course->thumbnail) }}" alt=""></td>
      <td>{{ $course->category->name }}</td>
      <td>Rp. {{ number_format($course->price) }}</td>
      <td>
        <a href="/course/{{$course->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
        <form onclick="return confirm('yakin ingin hapus?')" action="/course/{{$course->id}}" class="d-inline" method="post">
          @csrf
          @method('DELETE')
          <input type="submit" class="btn btn-danger btn-sm my-1" value="Delete">
        </form>
      </td>
    @endforeach
  </tr>
  </tbody>
  <tbody>
  	
  </tbody>
</table>
@endsection

@push('script')
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });

    @if(Session::has('success'))
      Swal.fire({
        title: "Berhasil!",
        text: "{{ session('success') }}",
        icon: "success",
      });
    @endif
  </script>
@endpush