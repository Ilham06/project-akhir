@extends('layouts.admin')

@section('title', 'Daftar Booking')

@section('content')
<table id="example1" class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>User</th>
      <th>Course</th>
      <th>Total</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
  	@foreach ($bookings as $booking)
      <tr>
        <td>{{ $booking->user->name }}</td>
        <td>{{ $booking->course->name }}</td> 
        <td>Rp. {{ number_format($booking->total) }}</td>
        <td>{{ $booking->status }}</td>
      </tr>
    @endforeach
  </tbody>
</table>
@endsection

@push('script')
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush