@extends('layouts.admin')

@section('title', 'Daftar Category')

@section('content')
<a href="/category/create" method="post" class="btn btn-success btn-sm mb-2">Tambah Data</a>

<table id="example1" class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>Nama Kategory</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($category as $item)
        <tr>
          <td>{{$item->name}}</td>
          <td>
            <a href="/category/{{$item->id}}/edit" class="btn btn-sm btn-primary">Edit</a>
            <form onclick="return confirm('yakin ingin hapus?')" action="/category/{{$item->id}}" method="post" class="d-inline">
                @csrf
                @method('DELETE')
                <input type="submit" class="btn btn-sm btn-danger my-1" value="Delete">
            </form>
          </td>
        </tr>
    @endforeach
  </tbody>
</table>
@endsection

@push('script')
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });

    @if(Session::has('success'))
      Swal.fire({
        title: "Berhasil!",
        text: "{{ session('success') }}",
        icon: "success",
      });
    @endif
  </script>
@endpush