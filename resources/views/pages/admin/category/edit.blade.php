@extends('layouts.admin')

@section('title', 'Edit Category')

@section('content')
<form action="/category/{{$category->id}}" method="post">
	@method('put')
	@csrf
	<div class="form-group">
		<label>Nama Kategory</label>
		<input type="text" 
		class="form-control" name="name" value="{{$category->name}}">

		@error('name')
			<small class="text-danger">{{ $message }}</small>
		@enderror
	</div>
	<button type="submit" class="btn btn-success mt-2">Simpan</button>
</form>
@endsection