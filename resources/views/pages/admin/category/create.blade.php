@extends('layouts.admin')

@section('title', 'Tambah Category')

@section('content')
<form action="/category" method="post">
	@csrf
	<div class="form-group">
		<label>Nama Kategory</label>
		<input type="text" class="form-control" name="name" placeholder="Masukan Nama">

		@error('name')
			<small class="text-danger">{{ $message }}</small>
		@enderror
	</div>
	<button type="submit" class="btn btn-success mt-2">Simpan</button>
</form>
@endsection