@extends('layouts.home')

@section('content')
    <!-- Page info -->
    <div class="page-info-section set-bg" data-setbg="{{ asset('frontend/img/bg.jpg')}}">
        <div class="container">
            <div class="site-breadcrumb">
                <a href="/index">Home</a>
                <span>Courses Detail</span>
            </div>
        </div>
    </div>
    <!-- Page info end -->


    <!-- single course section -->
    <section class="single-course spad pb-0">
        <div class="container">
            <div class="course-meta-area">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="course-note">Featured Course</div>
                        <h3>{{ $course->name }}</h3>
                        <div class="course-metas">
                            <div class="course-meta">
                                <div class="course-author">
                                    <div class="ca-pic set-bg" data-setbg="{{ asset('frontend/img/authors/2.jpg')}}"></div>
                                    <h6>Teacher</h6>
                                    <p>William Parker, <span>Developer</span></p>
                                </div>
                            </div>
                            <div class="course-meta">
                                <div class="cm-info">
                                    <h6>Category</h6>
                                    <p>{{ $course->category->name }}</p>
                                </div>
                            </div>
                            <div class="course-meta">
                                <div class="cm-info">
                                    <h6>Students</h6>
                                    <p>{{ count($course->booking) }} Registered Students</p>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="site-btn price-btn">Price: Rp. {{ number_format($course->price) }}</a>
                        <a href="/payment/{{ $course->id }}" class="site-btn buy-btn">Buy This Course</a>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <img src="{{ asset('images/'.$course->thumbnail)}}" alt="" class="course-preview">
            </div>
            <div class="row">
                <div class="col-lg-12 course-list">
                    <div class="cl-item">
                        <h4>Course Description</h4>
                        <p>{!! $course->description !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- single course section end -->


    <!-- Page -->
    <section class="realated-courses spad">
        <div class="course-warp">
            <h2 class="rc-title">Realated Courses</h2>
            <div class="rc-slider owl-carousel">
                <!-- course -->
                @foreach ($related as $element)
                    <div class="course-item">
                        <div class="course-thumb set-bg" data-setbg="{{ asset('images/'.$element->thumbnail)}}">
                            <div class="price">Price: Rp. {{ number_format($element->price) }}</div>
                        </div>
                        <div class="course-info">
                            <div class="course-text">
                                <a href="/detail/{{ $element->id }}"><h5>{{ $element->name }}</h5></a>
                                <p>{{ $element->category->name }}</p>
                                <div class="students">{{ count($element->booking) }} Students</div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Page end -->
@endsection