@extends('layouts.home')


@section('content')
<!-- Hero section -->
<section class="hero-section set-bg" data-setbg="{{ asset('frontend/img/bg.jpg')}}">
	<div class="container">
		<div class="hero-text text-white">
			<h2>Get The Best Free Online Courses</h2>
			<p>Get The Exacts Skills And Specialization You Need To Upgrade Skills. <br> Find Free Courses. Find Courses. Highlight: Webinars Available, Dmo Available </p>
		</div>
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<form class="intro-newslatter">
					<input type="text" placeholder="Name">
					<input type="text" class="last-s" placeholder="E-mail">
					<button class="site-btn"><a href="/signup" style="color: white;">Sign Up Now</a></button>
				</form>
			</div>
		</div>
	</div>
</section>
<!-- Hero section end -->


<!-- categories section -->
<section class="categories-section spad">
	<div class="container">
		<div class="section-title">
			<h2>Our Course Categories</h2>
			<p>Learn the skills you want to master with a learning flow that is arranged from basic to deep.</p>
		</div>
		<div class="row">
			<!-- categorie -->
			<div class="col-lg-4 col-md-6">
				<div class="categorie-item">
					<div class="ci-thumb set-bg" data-setbg="{{ asset('frontend/img/categories/1.jpg')}}"></div>
					<div class="ci-text">
						<h5>IT Development</h5>
						<p>Technologi or IT field is the person in charge of designing a system, be it from the structure, prospect, to the apperarance of the system itself</p>
					</div>
				</div>
			</div>
			<!-- categorie -->
			<div class="col-lg-4 col-md-6">
				<div class="categorie-item">
					<div class="ci-thumb set-bg" data-setbg="{{ asset('frontend/img/categories/2.jpg')}}"></div>
					<div class="ci-text">
						<h5>Web Design</h5>
						<p>A web designer is a person who has the expertise to make  website look attractive</p>
					</div>
				</div>
			</div>
			<!-- categorie -->
			<div class="col-lg-4 col-md-6">
				<div class="categorie-item">
					<div class="ci-thumb set-bg" data-setbg="{{ asset('frontend/img/categories/3.jpg')}}"></div>
					<div class="ci-text">
						<h5>Illustration & Drawing</h5>
						<p>An Illustration is a decoration, interpretation or visual explanation of a text, concept or process designer for integration in print and digital published media, such a posters, flyers, magazines, books, teaching materials, animations, video games and film</p>
					</div>
				</div>
			</div>
			<!-- categorie -->
			<div class="col-lg-4 col-md-6">
				<div class="categorie-item">
					<div class="ci-thumb set-bg" data-setbg="{{ asset('frontend/img/categories/4.jpg')}}"></div>
					<div class="ci-text">
						<h5>Social Media</h5>
						<p>An online medium, where users can easily participate, share, and create content including blogs, social networks, wikis, forums and virtual worlds</p>
					</div>
				</div>
			</div>
			<!-- categorie -->
			<div class="col-lg-4 col-md-6">
				<div class="categorie-item">
					<div class="ci-thumb set-bg" data-setbg="{{ asset('frontend/img/categories/5.jpg')}}"></div>
					<div class="ci-text">
						<h5>Photoshop</h5>
						<p>Image editor software made by Adobe System specifially for photo/image editing and effect creation</p>
					</div>
				</div>
			</div>
			<!-- categorie -->
			<div class="col-lg-4 col-md-6">
				<div class="categorie-item">
					<div class="ci-thumb set-bg" data-setbg="{{ asset('frontend/img/categories/6.jpg')}}"></div>
					<div class="ci-text">
						<h5>Cryptocurrencies</h5>
						<p>Currency that can be used for transactions between users without the need to go through a third party. In adddition to using it as a transaction tool, many users use cryptocurrency as an investment</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- categories section end -->


<!-- search section -->
<section class="search-section">
	<div class="container">
		<div class="search-warp">
			<div class="section-title text-white">
				<h2>Search your course</h2>
			</div>
			<div class="row">
				<div class="col-md-10 offset-md-1">
					<!-- search form -->
					<form class="course-search-form">
						<input type="text" placeholder="Course">
						<input type="text" class="last-m" placeholder="Category">
						<button class="site-btn">Search Couse</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- search section end -->


<!-- course section -->
<section class="course-section spad">
	<div class="container">
		<div class="section-title mb-0">
			<h2>Featured Courses</h2>
			<p>Online Classes from an ever-growing variety of themes. You can purchase a class <br> for perpetual access or rent a class for limites class.</p>
		</div>
	</div>
	<div class="course-warp">                                      
		<div class="row course-items-area">
			@foreach ($courses as $course)
				<!-- course -->
				<div class="mix col-lg-3 col-md-4 col-sm-6 finance">
					<div class="course-item">
						<div class="course-thumb set-bg" data-setbg="{{ asset('images/'.$course->thumbnail)}}">
							<div class="price">Price: Rp. {{ number_format($course->price) }}</div>
						</div>
						<div class="course-info">
							<div class="course-text">
								<a href="detail/{{ $course->id }}"><h5>{{ $course->name }}</h5></a>
								<p>{{ $course->category->name }}</p>
								<div class="students">{{ count($course->booking) }} Students</div>
							</div>
							{{-- <div class="course-author">
								<div class="ca-pic set-bg" data-setbg="{{ asset('frontend/img/authors/1.jpg')}}"></div>
								<p>William Parker, <span>Developer</span></p>
							</div> --}}
						</div>
					</div>
				</div>
				<!-- course -->
			@endforeach
			</div>
		</div>
	</div>
</section>
<!-- course section end -->


<!-- signup section -->
<section class="signup-section spad">
	<div class="signup-bg set-bg" data-setbg="{{ asset('frontend/img/signup-bg.jpg')}}"></div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6">
				<div class="signup-warp">
					<div class="section-title text-white text-left">
						<h2>Sign up to became a teacher</h2>
						</div>
					<!-- signup form -->
					<form class="signup-form">
						<input type="text" placeholder="Your Name">
						<input type="text" placeholder="Your E-mail">
						<input type="text" placeholder="Your Phone">
						<label for="v-upload" class="file-up-btn">Upload Course</label>
						<input type="file" id="v-upload">
						<button class="site-btn">Search Couse</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- signup section end -->


<!-- banner section -->
<section class="banner-section spad">
	<div class="container">
		<div class="section-title mb-0 pb-2">
			<h2>Join Our Community Now!</h2>
			<p>Let's Prove Itself! <br> That WebUni Can Helping You Develop Your Programming Skills</p>
		</div>
		<div class="text-center pt-5">
			<a href="/signup" class="site-btn">Register Now</a>
		</div>
	</div>
</section>
<!-- banner section end -->
@endsection