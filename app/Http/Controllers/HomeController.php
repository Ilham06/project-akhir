<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $courses = Course::take(4)->get();

        return view('pages.index', [
            'courses' => $courses
        ]);
    }

    public function about()
    {
        return view('pages.about');
    }

    public function courses()
    {
        $courses = Course::all();

        return view('pages.courses', [
            'courses' => $courses
        ]);
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function detail($id)
    {
        $course = Course::find($id);

        $related = Course::where('category_id', $course->category_id)->get();

        return view('pages.detail', [
            'course' => $course,
            'related' => $related
        ]);
    }

    public function payment($id)
    {
        $course = Course::find($id);

        return view('pages.payment', [
            'course' => $course
        ]);
    }

    public function checkout(Request $request)
    {
        $course = Course::find($request->course);

        Booking::create([
            'course_id' => $course->id,
            'user_id' => Auth::id(),
            'total' => $course->price,
            'status' => 'success'
        ]);

        return redirect('/')->with('success', 'Pembelian Berhasil Di Proses');
    }
}
