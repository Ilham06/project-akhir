<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    public function index()
    {
        $bookings = Booking::all();

        return view('pages.admin.booking.index', [
            'bookings' => $bookings
        ]);

    }
}
